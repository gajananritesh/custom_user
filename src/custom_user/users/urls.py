from django.urls import path
from django.views.generic.base import TemplateView
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.home_view , name='home'),
    path('profile/', views.profile_view , name='profile'),
    path('about/', views.about_view , name='about'),
    path('contactus/', views.contactus_view , name='contactus'),
    path('signup/', views.signup_view, name='signup'),
    path('map/', views.map_view, name='map'),
]
