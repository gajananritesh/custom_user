from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    first_name = models.CharField(max_length=20, blank=False, null=False)
    last_name = models.CharField(max_length=15, blank=False, null=False)
    username = models.CharField(max_length=20, blank=False, null=False, unique=True)
    email = models.EmailField(max_length=254)
    mobile_no = models.IntegerField(blank=True, null=True)
    address = models.CharField(blank=True, null=True, max_length=50)
    profile_image = models.ImageField(height_field='height_field', width_field='width_field', null=True, blank=True)
    height_field = models.IntegerField(default=0)
    width_field= models.IntegerField(default=0)

    def __str__(self):
        return self.email
 