from django.shortcuts import render,redirect
from django.urls import reverse_lazy
from django.views import generic
from .models import CustomUser
from .forms import CustomUserCreationForm

def signup_view(request):
    if request.method =='POST':
        form = CustomUserCreationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/users/login')
        else:
            form = CustomUserCreationForm()
            return render(request, 'signup.html',{'form':form, 'error_message':'Enter Valid Data.'})
    else:
        form = CustomUserCreationForm()
        return render(request, 'signup.html',{'form':form})

    
def home_view(request):
    if request.user.is_authenticated:
        r = request.user.username
        profile = CustomUser.objects.get(username=r)
        return render(request, 'home.html', {'profile':profile})
    else:
        return render(request, 'home.html')

def profile_view(request):
    if request.user.is_authenticated:
        r = request.user.username
        profile = CustomUser.objects.get(username=r)
        return render(request, 'profile.html', {'profile':profile})
    else:
        return render(request, 'home.html')

def about_view(request):
    return render(request, 'about.html')

def contactus_view(request):
    return render(request, 'contactus.html')

def map_view(request):
    return render(request, 'map.html')